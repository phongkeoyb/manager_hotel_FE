import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ServerHttpService {
  [x: string]: any;
  private httpOption = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  private HOST = 'http://localhost:8080/api/v1';
  constructor(private httpClient: HttpClient) { }

  public getCustomer(query?) {
    const url = `${this.HOST}/customer?q=${query}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public getCustomerById(id) {
    const url = `${this.HOST}/customer/${id}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public createCustomer(data) {
    const url = `${this.HOST}/customer`;
    return this.httpClient.post<any>(url, data, this.httpOption).pipe(catchError(this.handleError));
  }

  public updateCustomer(id, data) {
    const url = `${this.HOST}/customer/${id}`;
    return this.httpClient.put<any>(url, data, this.httpOption).pipe(catchError(this.handleError));
  }

  public deleteCustomer(id) {
    const url = `${this.HOST}/customer/${id}`;
    return this.httpClient.delete<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public getCity(code) {
    const url = `${this.HOST}/location/city?code=${code}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public getDistrict(code) {
    const url = `${this.HOST}/location/district?code=${code}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public getWard(code) {
    const url = `${this.HOST}/location/ward?code=${code}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }

  public exportExcelCustomer(query) {
    const url = `${this.HOST}/export_excel/customer?q=${query}`;
    return this.httpClient.get<any>(url, this.httpOption).pipe(catchError(this.handleError));
  }
}
