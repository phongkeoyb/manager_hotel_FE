import { NgModule } from '@angular/core';
import { BookingComponent } from './booking.component';
import { BookingRoutingModule } from './booking.routing.module';


@NgModule({
  imports: [BookingRoutingModule],
  declarations: [
    BookingComponent,
  ],
  exports: []
})
export class BookingModule { }
