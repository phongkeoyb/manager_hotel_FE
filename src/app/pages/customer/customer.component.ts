import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServerHttpService } from '@services/server-http.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
  public customers: [];
  inputValue?: string;
  inputValueChanged: Subject<string> = new Subject<string>();
  options: string[] = [];
  loading: boolean = true;

  constructor(private serverHttpService: ServerHttpService, private router: Router, private msg: NzMessageService) { }

  ngOnInit(): void {
    this.getCustomers();

    this.inputValueChanged.pipe(
      debounceTime(300),
      distinctUntilChanged())
      .subscribe(model => {
        this.getCustomers(model)
      });
  }

  public getCustomers(searchString?) {
    this.loading = true;
    this.serverHttpService.getCustomer(searchString || '').subscribe((res) => {
      this.loading = false;
      if (res.status) {
        this.customers = res.data;
      } else {
        this.customers = [];
      }
    }, err => {
      this.loading = false;
    })
  }

  onInput(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.inputValueChanged.next(value);
  }

  createCustomer() {
    this.router.navigate(['customer/create']);
  }

  editCustomer(id) {
    console.log(id);
    this.router.navigate(['customer/edit/' + id]);
  }

  deleteCustomer(id) {
    this.loading = true;
    this.serverHttpService.deleteCustomer(id).subscribe((res) => {
      this.loading = false;
      if (res.status) {
        this.msg.success(res.message)
        this.getCustomers()
      }
    }, err => {
      this.loading = false;
    })
  }

  exportExcelCustomer() {
    this.serverHttpService.exportExcelCustomer("").subscribe((res) => {
      this.loading = false;
      if (res.status) {
        this.msg.success(res.message)
        this.getCustomers()
      }
    }, err => {
      this.loading = false;
    })
  }
}
