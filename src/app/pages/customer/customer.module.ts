import { NgModule } from '@angular/core';
import { CustomerRoutingModule } from './customer.routing.module';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared';
import { RouterModule } from '@angular/router';

import { CustomerComponent } from './customer.component';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NzAutocompleteModule,
    RouterModule,
    CustomerRoutingModule
  ],
  declarations: [
    CustomerComponent,
    EditComponent,
    NewComponent,
  ],
  exports: []
})
export class CustomerModule { }
