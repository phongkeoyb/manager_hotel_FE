import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServerHttpService } from '@services/server-http.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  validateForm!: FormGroup;
  customerInfo: any;

  constructor(
    private fb: FormBuilder, 
    private location: Location,
    private serverHttpService: ServerHttpService,
    private msg: NzMessageService,
    private route: ActivatedRoute
  ) {
    
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      address: [null],
      descriptions: [null],
    });

    const id = this.route.snapshot.paramMap.get('id');
    this.getCustomer(id);
  }

  onBack() {
    this.location.back();
  }

  getCustomer(id) {
    if (!id) return;
    this.serverHttpService.getCustomerById(id).subscribe((res) => {
      if (res.status) {
        console.log(res)
        // this.msg.success(res.message)
        this.customerInfo = res.data;
        const { name, phone, address, descriptions } = res.data;
        this.validateForm.setValue({
          name, phone, address, descriptions
        });
      } else {
        this.msg.error(res.message || 'Get customer info error')
      }
    }, err => {
      this.msg.error(err.message || 'Get customer info error')
    })
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    this.updateCustomer();
  }

  updateCustomer() {
    if (!this.validateForm.valid) return;
    const { name, phone, address, descriptions } = this.validateForm.value;
    const data = {
      name, phone, address, descriptions,
    }
    this.serverHttpService.updateCustomer(this.customerInfo._id, data).subscribe((res) => {
      if (res.status) {
        console.log(res)
        this.msg.success(res.message)
        this.onBack()
      }
    })
  }

}
