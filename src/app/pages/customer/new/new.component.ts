import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServerHttpService } from '@services/server-http.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  validateForm!: FormGroup;
  public cities: [];
  public wards: [];
  public districts: [];

  public city: Number;
  public district: Number;
  public ward: Number;
  private timeoutDistrict: any;
  private timeoutWard: any;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private location: Location,
    private serverHttpService: ServerHttpService,
    private msg: NzMessageService,
  ) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      address: [null],
      descriptions: [null],
      city: [null],
      district: [null],
      ward: [null]
    });
    this.getCities();
  }

  onBack() {
    this.location.back();
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    this.createCustomer();
  }

  createCustomer() {
    if (!this.validateForm.valid) return;
    const { name, phone, address, descriptions, city, district, ward } = this.validateForm.value;
    const data = {
      name, phone, address, descriptions, city, district, ward
    }
    this.serverHttpService.createCustomer(data).subscribe((res) => {
      if (res.status) {
        this.msg.success(res.message)
        this.validateForm.reset();
      } else {
        this.msg.error(res.message)
      }
    })
  }

  getCities() {
    this.serverHttpService.getCity("").subscribe(res => {
      if (res.data) {
        this.cities = res.data;
      } else {
        this.cities = [];
      }
    })
  }

  getDistricts(code_city) {
    this.serverHttpService.getDistrict(code_city).subscribe(res => {
      if (res.data) {
        this.districts = res.data;
      } else {
        this.districts = [];
      }
    })
  }

  getWards(code_district) {
    this.serverHttpService.getWard(code_district).subscribe(res => {
      if (res.data) {
        this.wards = res.data;
      } else {
        this.wards = [];
      }
    })
  }

  onChangeCity(code_city) {
    this.districts = [];
    this.wards = [];
    this.validateForm.patchValue({
      district_id: null,
      ward_id: null
    })
    if (code_city) {
      this.getDistricts(code_city);
    }
  }

  onChangeDistrict(code_district) {
    this.wards = [];
    this.validateForm.patchValue({
      ward_id: null
    });
    if (code_district) {
      this.getWards(code_district);
    }
  }
  onChangeWard(ward_id) {
    return true
  }
}
